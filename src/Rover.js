'use strict'

/**
 * Class representing a rover.
 * @class Rover
 */
class Rover {
  /**
   * Creates an instance of Rover.
   * @param {object} startPosition - An object representing the start position for a rover.
   * @param {number} startPosition.x - The x coordinate of a rover's start position.
   * @param {number} startPosition.y - The y coordinate of a rover's start position.
   * @param {string} startPosition.heading - The heading of a rover's start position.
   * @param {string} instructions - The set of instructions for a rover.
   * @constructor
   * @memberof Rover
   */
  constructor(startPosition, instructions) {
    this._name = 'Rover'
    this._x = startPosition.x
    this._y = startPosition.y
    this._heading = startPosition.heading
    this._instructions = instructions
  }

  /**
   * Get the name of the rover.
   * @return {string} The name of the rover.
   * @readonly
   * @memberof Rover
   */
  get name() {
    return this._name
  }

  /**
   * Get the x position of the rover.
   * @return {number} The x position of the rover.
   * @readonly
   * @memberof Rover
   */
  get x() {
    return this._x
  }

  /**
   * Get the y position of the rover.
   * @return {number} The y position of the rover.
   * @readonly
   * @memberof Rover
   */
  get y() {
    return this._y
  }

  /**
   * Get the heading of the rover.
   * @return {number} The heading of the rover.
   * @readonly
   * @memberof Rover
   */
  get heading() {
    return this._heading
  }

  /**
   * Set a rover's name.
   * @param {string} name - The new name for a rover.
   * @memberof Rover
   */
  set name(name) {
    this._name = name
  }

  /**
   * Set a rover's x position.
   * @param {number} x - The new x position of a rover.
   * @memberof Rover
   */
  set x(x) {
    this._x = x
  }

  /**
   * Set a rover's y position.
   * @param {number} y - The new y position of a rover.
   * @memberof Rover
   */
  set y(y) {
    this._y = y
  }

  /**
   * Set a rover's heading.
   * @param {string} heading - The new heading of a rover.
   * @memberof Rover
   */
  set heading(heading) {
    this._heading = heading
  }

  /**
   * Process the instructions for a rover.
   * @param {Plateau} plateau - The plateau the rover is exploring.
   * @memberof Rover
   */
  processInstructions(plateau) {
    [...this._instructions].forEach(letter => {
      if (letter === 'M') {
        this.move(plateau)
      } else {
        this.setHeadingForNewOrientation(letter)
      }
    })
  }

  /**
   * Set the position of the rover on the plateau to the rover's name.
   * @param {Plateau} plateau - The plateau the rover is exploring.
   * @memberof Rover
   */
  setOnto(plateau) {
    plateau.setRover(this)
  }

  /**
   * Update a rover's position based on its heading and set it on the plateau.
   * @param {Plateau} plateau - The plateau the rover is exploring.
   * @memberof Rover
   */
  move(plateau) {
    let newX = -1
    let newY = -1

    switch (this.heading) {
      case 'N':
        newX = this.x
        newY = this.y + 1
        break
      case 'S':
        newX = this.x
        newY = this.y - 1
        break
      case 'E':
        newX = this.x + 1
        newY = this.y
        break
      case 'W':
        newX = this.x - 1
        newY = this.y
        break
      default:
        break
    }

    if (plateau.areCoordinatesInBounds(newX, newY)) {
      this.x = newX
      this.y = newY
      this.setOnto(plateau)
    } else {
      console.error(`New coordinates are out of bounds for Rover "${this.name}"`)
    }
  }

  /**
   * Set a rover's heading based on an orientation.
   * @param {letter} letter - The orientation for a rover. Can be 'L' or 'R'.
   * @memberof Rover
   */
  setHeadingForNewOrientation(letter) {
    switch (letter) {
      case 'L':
        switch (this._heading) {
          case 'N':
            this._heading = 'W'
            break
          case 'S':
            this._heading = 'E'
            break
          case 'E':
            this._heading = 'N'
            break
          case 'W':
            this._heading = 'S'
            break
          default:
            break
        }
        break
      case 'R':
        switch (this._heading) {
          case 'N':
            this._heading = 'E'
            break
          case 'S':
            this._heading = 'W'
            break
          case 'E':
            this._heading = 'S'
            break
          case 'W':
            this._heading = 'N'
            break
          default:
            break
        }
        break
      default:
        break
    }
  }
}

module.exports = Rover