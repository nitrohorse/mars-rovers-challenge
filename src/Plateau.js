'use strict'

/**
 * Class representing a plateau.
 * @class Plateau
 */
class Plateau {
  /**
   * Creates an instance of Plateau.
   * @param {number} width - The width of the plateau.
   * @param {number} height - The height of the plateau.
   * @constructor
   * @memberof Plateau
   */
  constructor(width, height) {
    this._width = width + 1
    this._height = height + 1

    this._plateau = new Array(this._height)
    for (let index = 0; index < this._height; index++) {
      this._plateau[index] = new Array(this._width).fill(' ')
    }
  }

  /**
   * Get the width of the plateau.
   * @return {number} The width of the plateau.
   * @readonly
   * @memberof Plateau
   */
  get width() {
    return this._width
  }

  /**
   * Get the height of the plateau.
   * @return {number} The height of the plateau.
   * @readonly
   * @memberof Plateau
   */
  get height() {
    return this._height
  }

  /**
   * Get the plateau itself.
   * @return {array} The plateau (2D array) itself.
   * @readonly
   * @memberof Plateau
   */
  get plateau() {
    return this._plateau
  }

  /**
   * Set the position of the rover on the plateau to the rover's name.
   * @param {Rover} rover - The rover to set.
   * @memberof Plateau
   */
  setRover(rover) {
    this._plateau[this._height - rover.y - 1][rover.x] = rover.name
  }

  /**
   * Log the plateau to the console.
   * @memberof Plateau
   */
  view() {
    console.log(this._plateau)
  }

  /**
   * Determine if x and y position is in bounds of plateau.
   * @param {number} x - The x position on the plateau.
   * @param {number} y - The y position on the plateau.
   * @return {boolean} True if position is in bounds, false if position is out of bounds.
   * @memberof Plateau
   */
  areCoordinatesInBounds(x, y) {
    return ((x < this._width && x >= 0) && (y < this._height && y >= 0))
  }
}

module.exports = Plateau