'use strict'

const fs = require('fs')
const Rover = require('./Rover')
const Rovers = require('./Rovers')
const Plateau = require('./Plateau')

/**
 * Read in the input from a file.
 * @return {array} An array of each line of input.
 */
const getInputFromFile = () => {
  // Confirm we got an input filename on the command line
  if (process.argv.length < 3) {
    console.warn(`Warning: Usage is yarn start FILENAME or npm run start FILENAME`)
    process.exit(0)
  }

  let inputArray = []
  try {
    const filename = process.argv[2]
    inputArray = fs.readFileSync(filename).toString().split('\n')
    // check that input is valid
  } catch (err) {
    console.error(`Error: File name not found`)
    process.exit(0)
  }
  return inputArray
}

/**
 * Create a new plateau based on the width and height from the input file.
 * @param {array} input - An array of each line of input.
 * @return {Plateau} A plateau.
 */
const setupPlateau = input => {
  const inputForPlateau = input.slice(0, 1)[0].split(' ')
  const width = Number(inputForPlateau[0])
  const height = Number(inputForPlateau[1])
  return new Plateau(width, height)
}

/**
 * Create a new group of rovers based on the start positions and instructions from the input file.
 * @param {array} input - An array of each line of input.
 * @return {Rovers} A group of rovers.
 */
const setupRovers = input => {
  const inputForRovers = input.slice(1)
  const rovers = new Rovers()
  for (let index = 0; index < inputForRovers.length; index += 2) {
    const startPositionArray = inputForRovers[index].split(' ')
    const instructions = inputForRovers[index + 1]

    const startPosition = {
      x: Number(startPositionArray[0]),
      y: Number(startPositionArray[1]),
      heading: startPositionArray[2]
    }
    rovers.add(new Rover(startPosition, instructions))
  }
  return rovers
}

const run = (() => {
  const inputArray = getInputFromFile()
  const plateau = setupPlateau(inputArray)
  const rovers = setupRovers(inputArray)

  rovers.deployOnto(plateau)
  rovers.explore(plateau)
  rovers.view()
  // plateau.view()
})()

