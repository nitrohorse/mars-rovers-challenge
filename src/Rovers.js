'use strict'

/**
 * Class representing a group of rovers.
 * @class Rovers
 */
class Rovers {
  /**
   * Creates an instance of Rovers.
   * @constructor
   * @memberof Rovers
   */
  constructor() {
    this._numberOfRovers = 0
    this._rovers = []
  }

  /**
   * Get the number of rovers in the group.
   * @return {number} The number of rovers.
   * @readonly
   * @memberof Rovers
   */
  get numberOfRovers() {
    return this._numberOfRovers
  }

  /**
   * Get the rovers in the group.
   * @return {array} The group of rovers.
   * @readonly
   * @memberof Rovers
   */
  get rovers() {
    return this._rovers
  }

  /**
   * Add a rover to the group.
   * Sets the name of the rover to the total number of rovers in the group.
   * @param {Rover} rover - The rover to add.
   * @memberof Rovers
   */
  add(rover) {
    this._numberOfRovers++
    rover.name = `${this._numberOfRovers}`
    this._rovers.push(rover)
  }

  /**
   * Deploy the rovers onto a plateau.
   * @param {Plateau} plateau - The plateau to deploy the rovers onto.
   * @memberof Rovers
   */
  deployOnto(plateau) {
    this._rovers.forEach(rover => {
      rover.setOnto(plateau)
    })
  }

  /**
   * Let each rover process its instructions and explore the plateau.
   * @param {Plateau} Plateau - The plateau for the rovers to explore.
   * @memberof Rovers
   */
  explore(plateau) {
    this._rovers.forEach(rover => {
      rover.processInstructions(plateau)
    })
  }

  /**
   * Log each rover's x and y position and heading to the console.
   * @memberof Rovers
   */
  view() {
    this._rovers.forEach(rover => {
      console.log(`${rover.x} ${rover.y} ${rover.heading}`)
    })
  }
}

module.exports = Rovers