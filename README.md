# Mars Rovers Challenge

[![Greenkeeper badge](https://badges.greenkeeper.io/nitrohorse/mars-rovers-challenge.svg)](https://greenkeeper.io/)
[![Build Status](https://travis-ci.org/nitrohorse/mars-rovers-challenge.svg?branch=master)](https://travis-ci.org/nitrohorse/mars-rovers-challenge)
[![devDependency Status](https://david-dm.org/nitrohorse/mars-rovers-challenge/dev-status.svg)](https://david-dm.org/nitrohorse/mars-rovers-challenge?type=dev)
[![License](https://img.shields.io/badge/license-GPLv3-yellow.svg)](https://github.com/nitrohorse/mars-rovers-challenge/blob/master/LICENSE)
[![Standard Style](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

* Challenge taken from [Google Code Archive](https://code.google.com/archive/p/marsrovertechchallenge/)

## Description

A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on-board cameras can get a complete view of the surrounding terrain to send back to Earth.

A rover's position and location is represented by a combination of x and y co-ordinates and a letter representing one of the four cardinal compass points. The plateau is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing North.

In order to control a rover, NASA sends a simple string of letters. The possible letters are 'L', 'R' and 'M'. 'L' and 'R' makes the rover spin 90 degrees left or right respectively, without moving from its current spot. 'M' means move forward one grid point, and maintain the same heading.

Assume that the square directly North from (x, y) is (x, y+1).

### Input

The first line of input is the upper-right coordinates of the plateau, the lower-left coordinates are assumed to be 0,0. The rest of the input is information pertaining to the rovers that have been deployed. Each rover has two lines of input. The first line gives the rover's position, and the second line is a series of instructions telling the rover how to explore the plateau.

The position is made up of two integers and a letter separated by spaces, corresponding to the x and y coordinates and the rover's orientation.

Each rover will be finished sequentially, which means that the second rover won't start to move until the first one has finished moving.

### Output

The output for each rover should be its final coordinates and heading.

### Test Input:
```bash
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
```

### Expected Output:
```bash
1 3 N
5 1 E
```

## Run Solution
* Install tools
  * [Node.js + npm](https://nodejs.org/en/)
  * [Yarn](https://yarnpkg.com/en/)
* Install dependencies
  * `yarn`
* Start
  * `yarn start <FILENAME>`
  * Example
    * `yarn start input.txt`

### Run Other Tasks
* Run tests
  * `yarn test`
* Run tests with coverage
  * `yarn test-with-coverage`
* Generate documentation into out/
  * `yarn docs`
* Bundle for distribution
  * `yarn bundle`
