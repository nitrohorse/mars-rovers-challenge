'use strict'

const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const expect = chai.expect
chai.use(sinonChai)

const Rover = require('../src/Rover')
const Rovers = require('../src/Rovers')
const Plateau = require('../src/Plateau')

describe('Plateau', () => {
  let plateau
  let width
  let height
  let upperRightX
  let upperRightY
  let rover

  beforeEach(() => {
    // Setup Plateau
    upperRightX = 5
    upperRightY = 5
    plateau = new Plateau(upperRightX, upperRightY)

    // Setup Rover
    const startPosition = {
      x: 0,
      y: 0,
      heading: 'N'
    }
    const instructions = 'L'
    rover = new Rover(startPosition, instructions)
  })

  describe('constructor', () => {
    it('should create a plateau (2D array) that allows the upper-right coordinates of the plateau to be accessible to a rover', () => {
      expect(plateau._plateau).to.eql(
        new Array(upperRightY + 1)
          .fill(new Array(upperRightX + 1)
            .fill(' '))
      )
    })
  })

  describe('setRover', () => {
    it('should write the name of the rover onto the plateau corresponding to the rover\'s position', () => {
      rover.x = 2
      rover.y = 1
      plateau.setRover(rover)
      expect(plateau._plateau[4][2]).to.equal(rover._name)
    })
  })

  describe('view', () => {
    it('should log the plateau to the console', () => {
      const stub = sinon.stub(console, 'log')
      plateau.view()
      expect(stub).to.have.been.calledWith(plateau._plateau)
      stub.restore()
    })
  })

  describe('areCoordinatesInBounds', () => {
    it('should return true if the x and y position are in bounds', () => {
      expect(plateau.areCoordinatesInBounds(0, 0)).to.be.true
      expect(plateau.areCoordinatesInBounds(5, 5)).to.be.true
    })

    it('should return false if the x position is out of bounds', () => {
      expect(plateau.areCoordinatesInBounds(6, 5)).to.be.false
    })
    it('should return false if the y position is out of bounds', () => {
      expect(plateau.areCoordinatesInBounds(0, -1)).to.be.false
    })
  })
})