'use strict'

const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const expect = chai.expect
chai.use(sinonChai)

const Rover = require('../src/Rover')
const Rovers = require('../src/Rovers')
const Plateau = require('../src/Plateau')

describe('Rovers', () => {
  let rover
  let rovers
  let currentNumberOfRovers

  beforeEach(() => {
    // Setup Rover
    const startPosition = {
      x: 0,
      y: 0,
      heading: 'N'
    }
    const instructions = 'L'
    rover = new Rover(startPosition, instructions)

    // Setup Rovers
    rovers = new Rovers()
    currentNumberOfRovers = 0
    rovers.add(rover)
  })

  describe('add', () => {
    it('should add a rover to the rover group', () => {
      // use eql to deeply compare values
      // http://www.chaijs.com/api/bdd/#method_eql
      expect(rovers._rovers).to.eql(Array(1).fill(rover))
    })

    it('should increment the total number of rovers in the gorup', () => {
      expect(rovers._numberOfRovers).to.equal(currentNumberOfRovers + 1)
    })

    it('should name the new rover to the new total number of rovers in the group', () => {
      expect(rover._name).to.equal(`${currentNumberOfRovers + 1}`)
    })
  })

  describe('deployOnto', () => {
    it('should call Plateau.setRover() for each rover in the group', () => {
      const stub = sinon.stub(Plateau.prototype, 'setRover')
      rovers.deployOnto(Plateau.prototype)
      expect(stub).to.have.been.calledOnce
      stub.restore()
    })
  })

  describe('explore', () => {
    it('should call Rover.processInstructions() for each rover in the group', () => {
      const stub = sinon.stub(Rover.prototype, 'processInstructions')
      rovers.explore(Plateau.prototype)
      expect(stub).to.have.been.calledOnce
      stub.restore()
    })
  })

  describe('view', () => {
    it('should log the x and y position and heading for each rover in the group', () => {
      const stub = sinon.stub(console, 'log')
      rovers.view()
      expect(stub).to.have.been.calledWith(`${rover.x} ${rover.y} ${rover.heading}`)
      stub.restore()
    })
  })
})