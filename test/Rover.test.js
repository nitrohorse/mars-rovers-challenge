'use strict'

const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const expect = chai.expect
chai.use(sinonChai)

const Rover = require('../src/Rover')
const Plateau = require('../src/Plateau')

describe('Rover', () => {
  let plateau
  let rover
  let upperRightX
  let upperRightY

  beforeEach(() => {
    // Setup Plateau
    upperRightX = 5
    upperRightY = 5
    plateau = new Plateau(upperRightX, upperRightY)

    // Setup Rover
    const startPosition = {
      x: 0,
      y: 0,
      heading: 'N'
    }
    const instructions = 'L'
    rover = new Rover(startPosition, instructions)
  })

  describe('processInstructions', () => {
    it('should call move() when "M" is processed', () => {
      rover._instructions = 'M'
      const stub = sinon.stub(Rover.prototype, 'move')
      rover.processInstructions(Rover.prototype)
      expect(stub).to.have.been.calledOnce
      stub.restore()
    })

    it('should call setHeadingForNewOrientation() when "L" or "R" are processed', () => {
      rover._instructions = 'L'
      const stub = sinon.stub(Rover.prototype, 'setHeadingForNewOrientation')
      rover.processInstructions(Rover.prototype)
      expect(stub).to.have.been.calledOnce
      stub.restore()
    })
  })

  describe('setOnto', () => {
    it('should call Plateau.setRover()', () => {
      const stub = sinon.stub(Plateau.prototype, 'setRover')
      rover.setOnto(plateau)
      expect(stub).to.have.been.calledOnce
      stub.restore()
    })
  })

  describe('move', () => {
    it('should increment the rover\'s y position if the heading is N', () => {
      rover._x = 1
      rover._y = 1
      rover._heading = 'N'
      rover.move(plateau)
      expect(rover._x).to.equal(1)
      expect(rover._y).to.equal(2)
    })

    it('should decrement the rover\'s y position if the heading is S', () => {
      rover._x = 1
      rover._y = 1
      rover._heading = 'S'
      rover.move(plateau)
      expect(rover._x).to.equal(1)
      expect(rover._y).to.equal(0)
    })

    it('should increment the rover\'s x position if the heading is E', () => {
      rover._x = 1
      rover._y = 1
      rover._heading = 'E'
      rover.move(plateau)
      expect(rover._x).to.equal(2)
      expect(rover._y).to.equal(1)
    })

    it('should decrement the rover\'s x position if the heading is W', () => {
      rover._x = 1
      rover._y = 1
      rover._heading = 'W'
      rover.move(plateau)
      expect(rover._x).to.equal(0)
      expect(rover._y).to.equal(1)
    })

    it('should not set the rover\'s x and y position if the new x and y position are out of bounds', () => {
      rover._x = 0
      rover._y = 0
      rover._heading = 'W'
      rover.move(plateau)
      expect(rover._x).to.equal(0)
      expect(rover._y).to.equal(0)
    })

    it('should log to the console if the rover\'s new x and y position are out of bounds', () => {
      rover._x = 0
      rover._y = 0
      rover._heading = 'W'

      const stub = sinon.stub(console, 'error')
      rover.move(plateau)
      expect(stub).to.have.been.calledWith(`New coordinates are out of bounds for Rover "${rover._name}"`)
      stub.restore()
    })

    it('should call Plateau.areCoordinatesInBounds()', () => {
      rover._x = 0
      rover._y = 0
      rover._heading = 'W'

      const stub = sinon.stub(Plateau.prototype, 'areCoordinatesInBounds')
      rover.move(plateau)
      expect(stub).to.have.been.called
      stub.restore()
    })

    it('should call setOnto() if x and y position are in bounds', () => {
      rover._x = 1
      rover._y = 1
      rover._heading = 'W'

      const stub = sinon.stub(Rover.prototype, 'setOnto')
      rover.move(plateau)
      expect(stub).to.have.been.called
      stub.restore()
    })
  })

  describe('setHeadingForNewOrientation', () => {
    it('should set heading to W if letter is L and current heading is N', () => {
      rover._heading = 'N'
      rover.setHeadingForNewOrientation('L')
      expect(rover._heading).to.equal('W')
    })

    it('should set heading to E if letter is L and current heading is S', () => {
      rover._heading = 'S'
      rover.setHeadingForNewOrientation('L')
      expect(rover._heading).to.equal('E')
    })

    it('should set heading to N if letter is L and current heading is E', () => {
      rover._heading = 'E'
      rover.setHeadingForNewOrientation('L')
      expect(rover._heading).to.equal('N')
    })

    it('should set heading to S if letter is L and current heading is W', () => {
      rover._heading = 'W'
      rover.setHeadingForNewOrientation('L')
      expect(rover._heading).to.equal('S')
    })

    it('should set heading to E if letter is R and current heading is N', () => {
      rover._heading = 'N'
      rover.setHeadingForNewOrientation('R')
      expect(rover._heading).to.equal('E')
    })

    it('should set heading to W if letter is R and current heading is S', () => {
      rover._heading = 'S'
      rover.setHeadingForNewOrientation('R')
      expect(rover._heading).to.equal('W')
    })

    it('should set heading to S if letter is R and current heading is E', () => {
      rover._heading = 'E'
      rover.setHeadingForNewOrientation('R')
      expect(rover._heading).to.equal('S')
    })

    it('should set heading to N if letter is R and current heading is W', () => {
      rover._heading = 'W'
      rover.setHeadingForNewOrientation('R')
      expect(rover._heading).to.equal('N')
    })
  })
})